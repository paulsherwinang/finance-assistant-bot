'use strict';

const sinon = require('sinon');
const nock = require('nock');

beforeEach(() => {
    global.sandbox = sinon.sandbox.create();
});

afterEach(() => {
    global.sandbox.restore();
    nock.cleanAll();
});
'use strict';

const nock = require('nock');
const server = require('../../../index');
const request = require('supertest').agent(server);

describe('GET /api/fb/subscribe', () => {
    it('should post to correct api', (done) => {
        let token = 'xxx';

        nock('https://graph.facebook.com')
            .get('/me/subscribed_apps')
            .query({ access_token: token })
            .reply(200, {
                data: [{
                    link: "https://www.facebook.com/games/?app_id=323700254736901",
                    name: "Finance Bot", id: "323700254736901"
                }]
            });

        request
            .get('/api/fb/subscribe')
            .query({ access_token: token })
            .expect(200, done);
    })
});
'use strict';

const expect = require('chai').expect;
const request = require('supertest');
const nock = require('nock');

const CONFIG = require('../../../config');
const bot = require('../../../bot');

describe('POST /api/buxfer/login', () => {
    require('../../testSetup.spec');

    it('should respond correctly when successful auth', (done) => {
        nock(CONFIG.BUXFER_API_BASEURL)
            .get('/api/login')
            .query((q) => {
                expect(q.userid).to.equal('username');
                expect(q.password).to.equal('password');
                return true;
            })
            .reply(200, {
                response: {
                    "status":"OK",
                    "token":"XXX"
                }
            });


        request(bot)
            .post('/api/buxfer/login')
            .send({ username: 'username', password: 'password' })
            .expect('Content-Type', /json/)
            .expect(200, done)
    });

    it('should respond correctly when not valid account', (done) => {
        nock(CONFIG.BUXFER_API_BASEURL)
            .get('/api/login')
            .query(true)
            .reply(200, {
                error: {
                    "request_id": 1,
                    "type": "client",
                    "message": "Email or username does not match an existing account."
                }
            });

        request(bot)
            .post('/api/buxfer/login')
            .send({ username: '', password: '' })
            .expect(401, done)
    });
});
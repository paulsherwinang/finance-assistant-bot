'use strict';

const nock = require('nock');
const CONFIG = require('../../../config');
const server = require('../../../index');
const request = require('supertest').agent(server);

describe('GET /api/buxfer/transactions', () => {
    require('../../testSetup.spec');

    it('should GET buxfer.com/api/transactions', (done) => {
        let token = 'xxx', start = '2017-08-09', end = '2017-08-10';

        nock(CONFIG.BUXFER_API_BASEURL)
            .get('/api/transactions')
            .query({ token: token, startDate: start, endDate: end })
            .reply(200, {
                "response": {
                    "status": "OK",
                    "numTransactions": 1334,
                    "transactions": [
                        {
                            "id": "2d510c2696ec50d19a4e122129c455df",
                            "description": "RECURRING TRANSFER REF #OPEQG7BT",
                            "date": "21 Feb",
                            "type": "income",
                            "amount": 25,
                            "accountId": "eca68525d89d2385dda040c3b5c571c2",
                            "tags": "transfer",
                        }
                    ]
                }
            });

        request
            .get('/api/buxfer/transactions')
            .query({ token: token, startDate: start, endDate: end })
            .expect(200, done);
    });
});
'use strict';

const nock = require('nock');
const CONFIG = require('../../../config');
const server = require('../../../index');
const request = require('supertest').agent(server);

describe('POST /api/buxfer/transactions', () => {
    require('../../testSetup.spec');

    it('should POST buxfer.com/api/add_transaction', (done) => {
        let token = 'xxx';

        nock(CONFIG.BUXFER_API_BASEURL)
            .post('/api/add_transaction')
            .query({
                token: token,
                format: 'sms',
                text: 'test description 100'
            })
            .reply(200, {
                "response": {
                    "status": "OK",
                    "transactionAdded": true,
                    "parseStatus": "success"
                }
            });

        request
            .post('/api/buxfer/transactions')
            .send({
                token: token,
                transactionDetails: {
                    description: 'test description',
                    amount: 100
                }
            })
            .expect(200, done);
    });
});
'use strict';

const reports = require('../../../components/resources/reports');
const expect = require('chai').expect;

describe('report.get common actions', () => {
    require ('../../testSetup.spec');

    describe.skip('_checkIfNeedsPagination', () => {
        it('should return if transaction total > 25', () => {
            let response = { numTransactions: '26' };
            let message = {};
            let bot = { reply: () => {} };
            let resolve = global.sandbox.spy();
            let r = reports._checkIfNeedsPagination(response, bot, message, resolve);

            expect(r).to.equal(response);
        });

        it('should resolve if < 25', () => {
            let response = { numTransactions: '24' };
            let message = {};
            let bot = { reply: global.sandbox.spy() };
            let resolve = global.sandbox.spy();

            reports._checkIfNeedsPagination(response, bot, message, resolve);

            expect(resolve.calledWith(response)).to.equal(true);
            expect(bot.reply.callCount).to.equal(1);
        });
    });

    it('_doPaginatedRequests should do paginated requests and return promises', () => {
        let response = { numTransactions: '52' };
        let user_data = { access_token: 'xxx' };
        let options = { 'test': 'test' };

        let p = reports._doPaginatedRequests(response, user_data, options);

        expect(p.length).to.equal(3);
    });
});
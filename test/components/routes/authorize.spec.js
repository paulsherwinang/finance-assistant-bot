'use strict';

const nock = require('nock');
const expect = require('chai').expect;
const server = require('../../../index');
const request = require('supertest').agent(server);

describe.skip('POST /authorize', () => {
    require ('../../testSetup.spec');

    it('should complete successfully for authorize endpoint', (done) => {
        let authCode = 'test_token';
        let redirectUrl = 'https://facebook.com/messenger_platform/account_linking/?account_linking_token=xxxxx';

        nock('https://www.buxfer.com')
            .get('/api/login')
            .query(true)
            .reply(200, {
                response: {
                    "status":"OK",
                    "token": authCode
                }
            });

        request
            .post('/authorize')
            .send({
                username: 'userid',
                password: 'pass'
            })
            .query({
                account_linking_token: 'xxxxx',
                redirect_uri: redirectUrl
            })
            .expect(302, (err, res) => {
                if(err) throw new Error(err);

                expect(res.header['location']).to.include(redirectUrl);
                expect(res.header['location']).to.include(authCode);
                done();
            });
    });

    it('should handle correctly if login api not successful', (done) => {
        let redirectUrl = 'https://facebook.com/messenger_platform/account_linking/?account_linking_token=xxxxx';

        nock('https://www.buxfer.com')
            .get('/api/login')
            .query(true)
            .reply(200, {
                error: {
                    "request_id": 12345,
                    "type":"XXX",
                    "message": "Auth FAiled"
                }
            });

        request
            .post('/authorize')
            .send({ username: 'userid', password: 'pass' })
            .query({
                account_linking_token: 'xxxxx',
                redirect_uri: redirectUrl
            })
            .expect(200,  done);
    });
});
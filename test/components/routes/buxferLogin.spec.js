'use strict';

const server = require('../../../index');
const request = require('supertest').agent(server);

describe('GET /login/buxfer', () => {
    require ('../../testSetup.spec');

    it('should render correct template', (done) => {
        request
            .get('/login/buxfer')
            .expect('Content-Type', /html/)
            .expect(200, done);
    });
});

'use strict';

module.exports = {
    MONGODB_URL: 'mongodb://localhost/financeBotAssistant',
    TEMPLATE_ENGINE: 'ejs',
    BUXFER_API_BASEURL: 'https://www.buxfer.com',
    VERIFY_TOKEN: 'finance_bot_will_be_awesome',
    CHATBOT_SERVER_PORT: 5000,
    PAGE_ACCESS_TOKEN: 'EAAEmZA1ayggUBAISL47MQgJKHPJIoGR10d55MJbBpF1tNas3ar5CYCph4sT4h8bh1VSZBwXlABDAD1qBfG53N8wYl9ZCQnARhuLIT2m6g8pddgMZCjYmHd4FhEYMdMTZCR7bIHsj2fBiM3cP4jr5gMZArBngYALCnJN8Bs0WX3DQZDZD',
    APIAI_DEVELOPER_ACCESS_TOKEN: '1e94114dfff54f6492b5bb9fd977f2c0',
    APIAI_CLIENT_ACCESS_TOKEN: '941b14ac26e94621b04cde87921a39ec',
    production: {
        CHATBOT_SERVER_PORT: 5000,
        CHATBOT_LOCALHOST_BASEURL: 'http://localhost',
        CHATBOT_FRONTEND_BASEURL: 'https://financebot.localtunnel.me'
    },
    development: {
        CHATBOT_SERVER_PORT: 5000,
        CHATBOT_LOCALHOST_BASEURL: 'http://localhost',
        CHATBOT_FRONTEND_BASEURL: 'https://financebot.localtunnel.me'
    }
};
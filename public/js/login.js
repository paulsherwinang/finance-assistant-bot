$(document).ready(function(){
    $('#buxferLoginForm').submit(function(event){
        event.preventDefault();

        var username = $('#buxferUsername').val();
        var password = $('#buxferPassword').val();

        $.ajax({
            method: 'POST',
            url: '/api/buxfer/login',
            data: { username: username, password: password },
            dataType: 'json'
        }).then(function(data) {
            var newLocation = '/authorize' + window.location.search;
            if (window.location.search) {
                newLocation += '&';
            } else {
                newLocation += '?';
            }
            window.location = newLocation + 'auth_token=' + data.response.token;

            console.log(window.location)
        }).catch(function(err) {
            console.log(err.status, err.responseJSON);
        });
    });
});
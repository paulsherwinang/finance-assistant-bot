'use strict';

const debug = require('debug')('financeBot:routes:authorize');
const authorize = require('../pages/controllers/authorize');

module.exports = function(webserver, controller) {
    debug('Configured POST /authorize for account auth');
    webserver.get('/authorize', authorize);
};
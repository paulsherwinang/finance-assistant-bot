'use strict';

const debug = require('debug')('financeBot:routes:buxfer');
const buxferLoginApi = require('../api/buxfer/apiBuxferLogin');
const buxferLoginPage = require('../pages/controllers/buxferLogin');

const transactionAdd = require('../api/buxfer/apiTransactionAdd');
const transactionGet = require('../api/buxfer/apiTransactionGet');

module.exports = function(webserver, controller) {
    debug('Configured GET /login/buxfer for buxfer login page');
    webserver.get('/login/buxfer', buxferLoginPage);

    debug('Configured POST /api/buxfer/login for buxfer login api');
    webserver.post('/api/buxfer/login', buxferLoginApi);

    debug('Configured POST /api/buxfer/transactions for buxfer');
    webserver.post('/api/buxfer/transactions', transactionAdd);

    debug('Configured GET /api/buxfer/transactions for buxfer');
    webserver.get('/api/buxfer/transactions', transactionGet);
};

'use strict';

const debug = require('debug')('financeBot:routes:facebook');
const apiSubscribe = require('../api/facebook/apiSubscribeGet');

module.exports = function(webserver, controller) {
    debug('Configured GET /api/fb/subscribe for fb app subscription');
    webserver.get('/api/fb/subscribe', apiSubscribe);
};
'use strict';

const debug = require('debug')('financeBot:routes:fbBotWebhook');

module.exports = function(webserver, controller) {

    debug('Configured POST /facebook/receive url for receiving events');
    webserver.post('/facebook/receive', (req, res) => {
        res.status(200).send();
        const bot = controller.spawn({});

        controller.handleWebhookPayload(req, res, bot);
    });

    debug('Configured GET /facebook/receive url for verification');
    webserver.get('/facebook/receive', (req, res) => {
        if (req.query['hub.mode'] === 'subscribe') {
            if (req.query['hub.verify_token'] === controller.config.verify_token) {
                res.send(req.query['hub.challenge']);
            } else {
                res.send('OK');
            }
        } else {
            res.status(400).send('Auth Failed')
        }
    });

};
'use strict';

const debug = require('debug')('financeBot:actions:reports');
const transactions = require('./transactions');
const _ = require('lodash');

module.exports = {
    get: (user_data, bot, message) => {
        return new Promise((resolve, reject) => {
            let options;

            if (_.includes(message.entities.date, '/')) {
                let range = message.entities.date.split('/');
                options = {startDate: range[0], endDate: range[1]};
            } else {
                options = {startDate: message.entities.date, endDate: message.entities.date};
            }

            bot.reply(message, {sender_action: 'typing_on'});
            transactions.query(user_data.account_token, options)
                .then((response) => {
                    debug('Checking if needed to paginated request...');
                    if(response.numTransactions > 25){
                        debug('Transaction is %s. need to paginate', response.numTransactions);
                        return response;
                    } else {
                        debug('Transaction is %s. No need to paginate', response.numTransactions);
                        bot.reply(message, { sender_action: 'typing_off' });
                        return resolve(response.transactions);
                    }
                })
                .then((response) => {
                    let nT = parseInt(response.numTransactions);
                    let requests = Math.ceil(nT/25);
                    let promises = [];

                    for(let i=0; i < requests; i++){
                        let paginatedOptions = Object.assign({}, options, { page: i+1 });
                        promises.push(transactions.query(user_data.account_token, paginatedOptions));
                    }
                    return Promise.all(promises);
                })
                .then((results) => {
                    debug('Resolving! paginated request... %j', results);
                    let transactions = _.map(results, (result) => {
                        return result.transactions;
                    });

                    bot.reply(message, { sender_action: 'typing_off' });
                    resolve(_.flatten(transactions));
                })
                .catch((apiErr) => {
                    reject(apiErr);
                });
        })
    }
};
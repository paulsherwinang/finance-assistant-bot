'use strict';

const request = require('request');
const CONFIG = process.env.NODE_ENV === 'production' ? require('../../config')['production'] : require('../../config')['development'];

const BASE_URL = `${CONFIG.CHATBOT_LOCALHOST_BASEURL}:${CONFIG.CHATBOT_SERVER_PORT}`;

module.exports = {
    query: (authToken, body) => {
        return new Promise((resolve, reject) => {
            request({
                method: 'GET',
                url: `${BASE_URL}/api/buxfer/transactions`,
                qs: Object.assign({}, { token: authToken }, body),
                json: true
            }, (err, response, body) => {
                if(err) return reject(err);
                resolve(body);
            });
        });
    },
    post: (authToken, transactionDetails) => {
        return new Promise((resolve, reject) => {
            request({
                method: 'POST',
                url: `${BASE_URL}/api/buxfer/transactions`,
                body: Object.assign({}, { token: authToken }, { transactionDetails: transactionDetails }),
                json: true
            }, (err, response, body) => {
                if(err) return reject(err);
                resolve(body);
            });
        })
    }
};
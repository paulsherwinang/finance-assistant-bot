'use strict';

const request = require('request');
const debug = require('debug')('financeBot:api:fb:subscribe_get');

module.exports = (req, res, next) => {
    let options = {
        method: 'GET',
        url: `https://graph.facebook.com/me/subscribed_apps`,
        qs: { access_token: req.query.access_token },
        json: true
    };

    request(options, (err, response, body) => {
        if (err){
            debug('Subscribe API Error 500');
            res.status(500).json(err);
            console.log(err)
            return;
        }

        if(body.data){
            debug('Successfully subscribed to Facebook events:', body);
            res.status(200).json(body);
        } else {
            debug('Error ocurred', body);
            res.status(400).json(body);
        }
    });

    debug('Called GET Subscribe API with Query: %j', options.qs);

};
'use strict';

const request = require('request');
const CONFIG = require('../../../config');
const debug = require('debug')('financeBot:api:buxfer:login');

let BUXFER_LOGIN_API = `${CONFIG.BUXFER_API_BASEURL}/api/login`;

module.exports = (req, res, next) => {
    debug('Calling buxfer login api...');

    let options = {
        uri: BUXFER_LOGIN_API,
        method: 'GET',
        qs: {
            userid: req.body.username,
            password: req.body.password
        },
        json: true
    };

    request(options, (error, response, body) => {
        if(!error) {
            debug('Call to buxfer login api successful, %j', body);

            if(body.response){
                debug('Authenticated Buxfer account! %j', body.response);
                res.status(200).json(body);
            } else {
                debug('Account credentials not found! %j', body.error);
                res.status(401).json(body);
            }
        } else {
            debug('Call to buxfer login api failed, %j', body);
            res.status(500).send(body);
        }
    });

    debug('Called API with query: %j', options.qs);

};
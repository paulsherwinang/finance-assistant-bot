'use strict';

const request = require('request');
const CONFIG = require('../../../config');
const debug = require('debug')('financeBot:api:buxfer:get_transaction');

let API_URL = `${CONFIG.BUXFER_API_BASEURL}/api/transactions`;

module.exports = (req, res, next) => {
    debug('Calling GET /api/buxfer/transactions...');

    let options = {
        uri: API_URL,
        method: 'GET',
        qs: req.query,
        json: true
    };

    request(options, (err, response, body) => {
        if(err) {
            debug('Call to transaction api failed, %j', err);
            res.status(500).send(err);
            return;
        }

        if(body.response){
            debug('API Call success: %j', body.response);
            res.status(200).json(body.response);
        } else {
            debug('Param error: %j', body.error);
            res.status(400).send(body.error);
        }
    });

    debug('Called API with query: %j', options.qs);

};
'use strict';

const request = require('request');
const CONFIG = require('../../../config');
const debug = require('debug')('financeBot:api:buxfer:add_transaction');

let API_URL = `${CONFIG.BUXFER_API_BASEURL}/api/add_transaction`;

module.exports = (req, res, next) => {
    debug('Calling POST /api/buxfer/transactions...');

    let bodyToText = (details) => {
        return `${details.description} ${details.amount}`
    };

    let options = {
        uri: API_URL,
        method: 'POST',
        qs: {
            token: req.body.token,
            format: 'sms',
            text: bodyToText(req.body.transactionDetails)
        },
        json: true
    };

    request(options, (error, response, body) => {
        if(!error) {
            debug('Call to transactions successful, %j', body);

            if(body.response){
                debug('Added transaction! %j', body.response);
                res.status(200).json(body.response);
            } else {
                debug('Account credentials not found! %j', body.error);
                res.status(401).json(body.error);
            }
        } else {
            debug('Call to buxfer add_transaction api failed, %j', body);
            res.status(500).send(body);
        }
    });

    debug('Called API with query: %j', options.qs);

};
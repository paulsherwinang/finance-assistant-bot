'use strict';

const request = require('request');
const debug = require('debug')('financeBot:subscribeEvents');
const CONFIG = process.env.NODE_ENV === 'production' ? require('../config')['production'] : require('../config')['development'];

const BASE_URL = `${CONFIG.CHATBOT_LOCALHOST_BASEURL}:${CONFIG.CHATBOT_SERVER_PORT}`;

module.exports = function(controller) {
    let options = {
        method: 'GET',
        url: `${BASE_URL}/me/subscribed_apps`,
        qs: {
            access_token: controller.config.access_token
        }
    };

    request(options, (err, res, body) => {
        if (err) {
            throw new Error(err);
        } else {
            controller.startTicking();
        }
    });
};
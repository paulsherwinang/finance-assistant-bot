'use strict';

const debug = require('debug')('financeBot:intent:report.get');
const numeral = require('numeral');
const apiai = require('../apiaiBotkitMiddleware');

const reports = require('../resources/reports');
const transactionHelpers = require('../../helpers/transactionHelpers');
const dateHelpers = require('../../helpers/dateHelpers');
debug('Listening to report.get intent');

module.exports = (controller) => {

    controller.hears([
        'report.get.expense',
        'report.get.income'
    ], 'message_received', apiai.hears, (bot, message) => {
        let isExpense = message.entities.verb === 'expense';

        debug('REPORT.GET INTENT VERB: %s', message.entities.verb);
        let replyMessage = message.fulfillment.speech;

        if(!message.entities.date || !dateHelpers.checkValidDate(message.entities.date)){
            bot.reply(message, replyMessage);
            return;
        }

        controller.storage.users.get(message.user, (err, user_data) => {
            if(err) throw err;

            reports.get(user_data, bot, message)
                .then((transactions) => {
                    let amount;

                    if(isExpense){
                        amount = transactionHelpers.getExpenseTotal(transactions);
                    } else {
                        amount = transactionHelpers.getIncomeTotal(transactions);
                    }

                    let strAmount = numeral(amount).format('0,0.00');
                    let replaced = replyMessage.replace('<AMOUNT>', strAmount);
                    bot.reply(message, replaced);
                })
                .catch((message) => {
                    bot.reply(message, 'An Error ocurred');
                });
        });

    });
};
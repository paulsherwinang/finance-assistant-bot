'use strict';

const debug = require('debug')('financeBot:events:messageReceived');
const messageHelpers = require('../../helpers/messageHelpers');

debug('Listening to message_received');

module.exports = (controller) => {
    controller.on('message_received', (bot, message) => {
        debug('Message received: %s', message.text);

        // -------
        // stub for account token. Please remove sometime.
        // -------
        controller.storage.users.save({
            id: message.user,
            account_token: process.env.BUXFER_TOKEN
        }, () => {});
    });
};
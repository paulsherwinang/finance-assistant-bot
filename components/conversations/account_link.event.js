'use strict';

const debug = require('debug')('financeBot:events:accountLinking');

debug('Listening to account_linking');

module.exports = (controller) => {
    controller.on('account_linking', (bot, message) => {
        debug('Account Linked: %s', message.text);
    });
};
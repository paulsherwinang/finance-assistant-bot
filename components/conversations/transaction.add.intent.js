'use strict';

const debug = require('debug')('financeBot:intent:transaction.add');
const apiai = require('../apiaiBotkitMiddleware');
const transactions = require('../resources/transactions');

debug('Listening to transaction.add intent');

module.exports = (controller) => {
    controller.hears([
        'transaction.add.expense',
        'transaction.add.income'
    ], 'message_received', apiai.hears, (bot, message) => {
        let isExpense = message.entities.verb === 'expense';

        debug('TRANSACTION.ADD INTENT: %j', message.entities);
        let replyMessage = message.fulfillment.speech;

        if(!message.entities.description || !message.entities.amount){
            bot.reply(message, replyMessage);
            return;
        }

        controller.storage.users.get(message.user, (err, user_data) => {
            if (err) throw err;

            let amount = message.entities.amount;
            let description = message.entities.description;

            transactions.post(user_data.account_token, {
                description: description,
                amount: isExpense ? amount : `+${amount}`
            }).then((response) => {
                if(response.transactionAdded) bot.reply(message, replyMessage);
            }).catch((errorMessage) =>{
                bot.reply(message, 'There was an error adding your transaction. Please try again');
            })
        });
    });
};



'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const querystring = require('querystring');
const timeout = require('connect-timeout');
const expressValidator = require('express-validator');
const debug = require('debug')('financeBot:webserver');
const CONFIG = require('../config');

const bsRoutes = (webserver, controller) => {
    // import all the pre-defined routes that are present in /components/routes
    const routeBasePath = require("path").join(__dirname, "routes");
    require("fs").readdirSync(routeBasePath).forEach(function(file) {
        require("./routes/" + file)(webserver, controller);
    });
};

const bsMiddlewares = (webserver, controller) => {
    // import express middlewares that are present in /components/express_middleware
    const middlewareBasePath = require("path").join(__dirname, "express_middleware");
    require("fs").readdirSync(middlewareBasePath).forEach(function(file) {
        require("./express_middleware/" + file)(webserver, controller);
    });
};

const bsTimeoutMiddleware = (webserver, controller) => {
    webserver.use(timeout('20s'));
    webserver.use((req, res, next) => {
        if (!req.timedout) next();
    });
};

const bsViewsSettings = (webserver, controller) => {
    webserver.set('view engine', CONFIG.TEMPLATE_ENGINE);
    webserver.set('views', './components/pages/views');
};

module.exports = (controller, bot) => {
    debug('Configuring Express webserver...');
    const webserver = express();

    webserver.use(bodyParser.json());
    webserver.use(bodyParser.urlencoded({ extended: true }));
    webserver.use(expressValidator());

    webserver.use(express.static('public'));

    bsMiddlewares(webserver, controller);
    bsViewsSettings(webserver, controller);
    bsTimeoutMiddleware(webserver, controller);
    bsRoutes(webserver, controller);

    controller.webserver = webserver;

    return webserver;
};
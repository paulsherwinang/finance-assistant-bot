'use strict';

const CONFIG = require('../config');

module.exports = require('botkit-middleware-apiai')({
    token: CONFIG.APIAI_DEVELOPER_ACCESS_TOKEN,
    skip_bot: false
});
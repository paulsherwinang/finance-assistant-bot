'use strict';

const request = require('request');
const loginHelpers = require('../../../helpers/loginHelpers');
const debug = require('debug')('financeBot:api:authorize');

module.exports = (req, res, next) => {
    debug('Called buxfer authorize api. Query: %j', req.query);
    let redirectUri = `${req.query.redirect_uri}&authorization_code=${req.query.auth_token}`;
    res.redirect(redirectUri);
};
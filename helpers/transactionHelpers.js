'use strict';

const _ = require('lodash');

module.exports = {
    getExpenseTotal: (transactions) => {
        let expenses = _.filter(transactions, { type: 'expense' });
        return expenses.reduce((memo, transaction) => {
            return memo + transaction.expenseAmount;
        }, 0);
    },
    getIncomeTotal: (transactions) => {
        let expenses = _.filter(transactions, { type: 'income' });
        let total = expenses.reduce((memo, transaction) => {
            return memo + transaction.expenseAmount;
        }, 0);

        return Math.abs(total);
    }
};
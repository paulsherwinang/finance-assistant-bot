'use strict';

module.exports = {
    getPostbackButton: (title, payload) => {
        return {
            'type': 'postback',
            'title': title, //20 char
            'payload': payload, //1000 char
        }
    },
    getLoginButton: (url) => {
        return {
            "type": "account_link",
            "url": url
        }
    },
    getLogoutButton: () => {
        return {
            "type": "account_unlink"
        }
    },
    getTemplateButtons: (text, buttons) => {
        return {
            'type': 'template',
            'payload': {
                'template_type': 'button',
                'text': text,
                'buttons': buttons
            }
        }
    }
};
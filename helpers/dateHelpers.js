'use strict';

const moment = require('moment');

module.exports = {
    checkValidDate: (string) => {
        return moment(string, 'YYYY-MM-DD/YYYY-MM-DD');
    }
};
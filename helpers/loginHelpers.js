'use strict';

const request = require('request');
const CONFIG = process.env.NODE_ENV === 'production' ? require('../config')['production'] : require('../config')['development'];

const BASE_URL = `${CONFIG.CHATBOT_LOCALHOST_BASEURL}:${CONFIG.CHATBOT_SERVER_PORT}`;

module.exports = {
    buxferLogin: (userId, pass) => {
        return new Promise((resolve, reject) => {
            request({
                method: 'POST',
                url: `${BASE_URL}/api/buxfer/login`,
                body: {
                    username: userId,
                    password: pass
                },
                json: true
            }, (err, response, body) => {
                if(err) return reject(err);
                resolve(body);
            })
        });
    }
};
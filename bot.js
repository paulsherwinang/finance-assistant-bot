'use strict';

require('dotenv').config();

const debug = require('debug')('financeBot:main');
const Botkit = require('botkit');
const apiai = require('./components/apiaiBotkitMiddleware');
const CONFIG = require('./config');

const bsConversations = () => {
    const normalizedPath = require("path").join(__dirname, "components/conversations");
    require("fs").readdirSync(normalizedPath).forEach(function(file) {
        require("./components/conversations/" + file)(controller);
    });
};

const controller = Botkit.facebookbot({
    debug: true,
    access_token: CONFIG.PAGE_ACCESS_TOKEN,
    verify_token: CONFIG.VERIFY_TOKEN
});

bsConversations();
require(__dirname + '/components/subscribeEvents.js')(controller);

controller.middleware.receive.use(apiai.receive);

module.exports = require(__dirname + '/components/expressWebserver.js')(controller);


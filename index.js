'use strict';

const PORT = require('./config').CHATBOT_SERVER_PORT;
const bot = require('./bot');

module.exports = bot.listen(PORT, () => {
    console.log('Access the [FinanceBot] at port %s', PORT)
});

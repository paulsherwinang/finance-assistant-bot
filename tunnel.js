'use strict';

const localtunnel = require('localtunnel');
const CONFIG = require('./config');
const bot = require('./bot');

const PORT = CONFIG.CHATBOT_SERVER_PORT;

bot.listen(PORT, () => {
    localtunnel(PORT, {subdomain: 'financebot'},  function(err, tunnel){
        console.log('Access the bot API on the tunnel: %s', tunnel.url)
    });
});
